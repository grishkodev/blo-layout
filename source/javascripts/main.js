let $window = $(window);
let $document = $(document);
let $html = $(document.documentElement);
let $body = $(document.body);


$document.ready(() => {

  $('.hamburger').on('click', function(event) {
    $(this).toggleClass('is-active');
    $('.main-menu').toggleClass('is-open');
  });

  $window.resize(() => {

  });

  $window.scroll(() => {

    // sticky header
    let siteHeader = $('.header');
    let scrollDistance = $(window).scrollTop();

    if (scrollDistance >= 150)
      siteHeader.addClass('fixed');
    else
      siteHeader.removeClass('fixed');

    // scroll to anchor
    $('a[href^="#"]').on('click', function(event) {
      let target = $(this.getAttribute('href'));
      if(target.length) {
        event.preventDefault();
        $('html, body').stop().animate({
          scrollTop: target.offset().top
        }, 800);
      }
    });

    // mainmenu active link
    $('section[id]').each(function (i) {
      if ($(this).position().top - 180 <= scrollDistance) {
        $('.main-menu a[href*="#"]:not([href="#"]).is-active').removeClass('is-active');
        $('.main-menu a').eq(i).addClass('is-active');
      }
      if (scrollDistance < 300) {
        $('.main-menu a').removeClass('is-active');
      }
    });

  });

});
